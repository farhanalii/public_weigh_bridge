# -*- coding: utf-8 -*-
import logging

from flectra import models, api, fields, osv
from flectra.exceptions import ValidationError
import xmlrpc.client as xc
import random
from typing import List
import serial, time, sys
import serial
import io
import time
import datetime
from serial.tools import list_ports
_logger = logging.getLogger(__name__)


class weight_bridge(models.Model):
    _name = 'weight.bridge'

    state = fields.Selection([('draft', 'Draft'), ('validate', 'Validate')], default = 'draft')

    gatepass = fields.Char(required=True)
    customer = fields.Many2one('res.partner')
    product = fields.Many2one('product.product')
    vehicle = fields.Many2one('private.vehicle')
    first_weight = fields.Float('First Weight')
    second_weight = fields.Float()
    net_weight = fields.Float(compute="compute_netweight", readonly=True, store=True)
    driver_name = fields.Char("Driver Name")

    @api.onchange('vehicle')
    def get_first_weight(self):
        if self.vehicle:
            weight = self.env['private.vehicle'].search([('name', "=", self.vehicle.name)])
            self.first_weight = weight.vehicle_weight

    @api.depends('first_weight', 'second_weight')
    @api.multi
    def compute_netweight(self):
        for record in self:
            total = record.second_weight - record.first_weight
            record.net_weight = total

    @api.multi
    def validate(self):
        self.state = 'validate'
        print('sssssssssssssssssssssssssssssssss')

    def get_port(self,ports):
        print("farhan")
        for port_no, description, address in ports:  # each port has three arguments PORT_NUMBER , DESCRIPTION, ADDRESS
            #  print(port_no , description , address)
            if 'COM' in address:  # The attached port has address USB other None
                return port_no  # return our port_number

    @api.multi
    def get_data_from_port(self):

        try:

            scale = serial.Serial('/dev/ttyS0', baudrate=1200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE,
                                  stopbits=serial.STOPBITS_ONE)

            print(scale.is_open)  # True for open

            while True:
                input = scale.read()
                if input == b'\x02':  # start character
                    input = scale.read(7)  # read next seven characters : (sign+- and 6 digits)
                    print(input.decode("ascii"))  # Display weight received in ASCII
                    break

        except:
            raise ValidationError("weigh bridge is not connected")


    # @api.onchange('gatepass')
    def get_data_from_ghani_db(self):

        # Assigning variables
        url = 'https://ghani.axm.app'
        db = 'ghani_testing'
        username = 'axiom'
        password = 'axiom123'

        # create connection
        common = xc.ServerProxy('{}/xmlrpc/2/common'.format(url))
        uid = common.authenticate(db, username, password, {})
        models = xc.ServerProxy('{}/xmlrpc/2/object'.format(url))
        # Getting data from Ghani_main_DB
        product_id = models.execute_kw(db, uid, password,
                                       'sale.order.line', 'search_read',
                                       [[['product_id', '!=', self.product.id]]],
                                       {'fields': ['product_id'],})

        for prod in product_id:
            product_obj = self.env['product.product'].search([('default_code','=',prod['product_id'][1].split('[')[1].split(']')[0])])
            if not product_obj:
                all = self.env['product.category'].search(
                    [('name', '=', 'All')])
                product_obj.create({
                    'name' : prod['product_id'][1].split('[')[1].split(']')[1],
                    'default_code' : prod['product_id'][1].split('[')[1].split(']')[0],
                    'type' : 'product',
                    'categ_id' : all.id,
                })


        data_from_ghani = models.execute_kw(db, uid, password,
                                            'stock.picking', 'search_read',
                                            [[['name', '!=', 'WH/OUT/'+str(self.gatepass)], ['state', '=', 'deliveryorder']]],
                                            {'fields': ['name','partner_id','vehicle_no','empty_weight', 'driver_name', 'driver_cnic', 'product_id', 'driver_name', 'customer_cnic'],})

        for ghani_data in data_from_ghani:
            res = False
            product_obj = self.env['product.product'].search(
                [('default_code', '=', ghani_data['product_id'][1].split('[')[1].split(']')[0])])
            customer = self.env['res.partner'].search([('cnic', '=', ghani_data['customer_cnic'])])
            if not customer:
                res = customer.create({
                    'name' : ghani_data['partner_id'][1],
                    'cnic': ghani_data['customer_cnic'],
                })
            else:
                res = customer

            veh = False
            vehicle = self.env['private.vehicle'].search([('name', '=', ghani_data['vehicle_no'][1])])
            if not vehicle:
                veh = vehicle.create({
                    'name': ghani_data['vehicle_no'][1],
                    'vehicle_weight': ghani_data['empty_weight'],
                })
            else:
                veh = vehicle

            weight_bridge_data = self.env['weight.bridge'].search([('gatepass','=', ghani_data['name'].split('/')[-1])])
            if not weight_bridge_data:
                weight_bridge_data.create({
                    'gatepass' : ghani_data['name'].split('/')[-1],
                    'customer' : res.id,
                    'vehicle' : veh.id,
                    'first_weight' : ghani_data['empty_weight'],
                    'driver_name' : ghani_data['driver_name'],
                    'product' : product_obj.id,
                })

    def get_data_from_mohandus_db(self):

        # Assigning variables
        url = 'https://ghani.axm.app'
        db = 'mohandus_testing'
        username = 'axiom'
        password = 'axiom'

        # create connection
        common = xc.ServerProxy('{}/xmlrpc/2/common'.format(url))
        uid = common.authenticate(db, username, password, {})
        models = xc.ServerProxy('{}/xmlrpc/2/object'.format(url))
        # Getting data from Ghani_main_DB
        product_id = models.execute_kw(db, uid, password,
                                       'sale.order.line', 'search_read',
                                       [[['product_id', '!=', self.product.id]]],
                                       {'fields': ['product_id'],})

        for prod in product_id:
            product_obj = self.env['product.product'].search([('default_code','=',prod['product_id'][1].split('[')[1].split(']')[0])])
            if not product_obj:
                all = self.env['product.category'].search(
                    [('name', '=', 'All')])
                product_obj.create({
                    'name' : prod['product_id'][1].split('[')[1].split(']')[1],
                    'default_code' : prod['product_id'][1].split('[')[1].split(']')[0],
                    'type' : 'product',
                    'categ_id' : all.id,
                })


        data_from_ghani = models.execute_kw(db, uid, password,
                                            'stock.picking', 'search_read',
                                            [[['name', '!=', 'WH/OUT/'+str(self.gatepass)], ['state', '=', 'deliveryorder']]],
                                            {'fields': ['name','partner_id','vehicle_no','empty_weight', 'driver_name', 'driver_cnic', 'product_id', 'driver_name', 'customer_cnic'],})

        for ghani_data in data_from_ghani:
            res = False
            product_obj = self.env['product.product'].search(
                [('default_code', '=', ghani_data['product_id'][1].split('[')[1].split(']')[0])])
            customer = self.env['res.partner'].search([('cnic', '=', ghani_data['customer_cnic'])])
            if not customer:
                res = customer.create({
                    'name' : ghani_data['partner_id'][1],
                    'cnic': ghani_data['customer_cnic'],
                })
            else:
                res = customer

            veh = False
            vehicle = self.env['private.vehicle'].search([('name', '=', ghani_data['vehicle_no'][1])])
            if not vehicle:
                veh = vehicle.create({
                    'name': ghani_data['vehicle_no'][1],
                    'vehicle_weight': ghani_data['empty_weight'],
                })
            else:
                veh = vehicle

            weight_bridge_data = self.env['weight.bridge'].search([('gatepass','=', ghani_data['name'].split('/')[-1])])
            if not weight_bridge_data:
                weight_bridge_data.create({
                    'gatepass' : ghani_data['name'].split('/')[-1],
                    'customer' : res.id,
                    'vehicle' : veh.id,
                    'first_weight' : ghani_data['empty_weight'],
                    'driver_name' : ghani_data['driver_name'],
                    'product' : product_obj.id,
                })





       # # do_record = self.env['stock.picking'].search([])
       #  do_record = models.execute_kw(db, uid, password, 'stock.picking', 'search_read', [[['id', '>', 0]]])
       #
       #  for recx in do_record:
       #      if recx:
       #          rec = self.env['weight.bridge'].search([('gatepass', '=', recx['name'].split('/')[-1])])
       #          # rec = models.execute_kw(db, uid, password, 'weight.bridge', 'search_read',
       #          #                         [[['gatepass', '=', recx.name.split('/')[-1]]]],
       #          #                         {'fields': ['product', 'net_weight', 'gatepass', 'vehicle', ]})
       #          if not rec:
       #              continue
       #
       #          # sale_order_id = self.env['sale.order'].search([('name', '=', recx[0].origin)])
       #          sale_order_id = models.execute_kw(db, uid, password, 'sale.order', 'search_read', [[['name', '=', recx['origin']]]])
       #          # sale_order_line_id = self.env['sale.order.line'].search([("order_id", "=", sale_order_id.name)])
       #          sale_order_line_id =  models.execute_kw(db, uid, password, 'sale.order.line', 'search_read',
       #                                  [[["order_id", "=", sale_order_id[0]['name']]]])
       #
       #          # product = self.env['product.product'].search(
       #          #     [('default_code', '=', rec[0]['product'][1].split('[')[1].split(']')[0])])
       #          product = models.execute_kw(db, uid, password, 'product.product', 'search_read',
       #                                      [[["default_code", "=",
       #                                         recx['product_id'][1].split('[')[1].split(']')[0]]]])
       #
       #          veh = self.env['private.vehicle'].search(
       #              [('name', '=', rec[0]['vehicle'][1])])
       #          veh = models.execute_kw(db, uid, password, 'private.vehicle', 'search_read',
       #                                  [['name', '=', rec[0]['vehicle'][1]]])
       #
       #          sale_order_line_id.write({
       #              'qty_delivered': rec[0]['net_weight'],
       #              'product_id': product.id,
       #          })
       #
       #          stock_picking = self.env['stock.picking'].search([('origin', '=', recx[0].origin)])
       #          print("+++++++++++", stock_picking)
       #          # stock_move = self.env['stock.move'].search([('picking_id','=', stock_picking[0].id)])
       #          stock_picking.write({
       #              'vehicle_no': veh.id
       #          })
       #          for line in stock_picking.move_lines:
       #              print("--------------", line)
       #              print("1111111111", line.quantity_done)
       #              line.write({
       #                  'quantity_done': rec[0]['net_weight'],
       #                  'product_id': product.id,
       #              })
       #              print("22222222222", line.quantity_done)
       #      else:
       #          print("farhan")